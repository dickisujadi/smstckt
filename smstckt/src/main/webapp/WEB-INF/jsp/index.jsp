<!DOCTYPE html>
<html>
    <head>
        <title>SpringBoot CRUD</title>
        <style>
            table {
                font-family: arial, sans-serif;
                border-collapse: collapse;
             width: 100%;
            }

            td, th {
                border: 1px solid #dddddd;
                text-align: left;
                padding: 8px;
            }

            tr:nth-child(even) {
                background-color: #dddddd;
            }
        </style>
    </head>
<body>

<h2>HTML Table</h2>

<table>
  <tr>
    <th>id</th>
    <th>name</th>
    <th>id_bts</th>
    <th>id_ticket</th>
    <th>number</th>
  </tr>
    <c:forEach var = "list" items="${lists}">
  <tr>
    <td>${list.id}</td>
    <td>${list.name}</td>
    <td>${list.id_bts}</td>
    <td>${list.id_ticket}</td>
    <td>${list.number}</td>
  </tr>
  </c:forEach>
</table>

</body>
</html>
