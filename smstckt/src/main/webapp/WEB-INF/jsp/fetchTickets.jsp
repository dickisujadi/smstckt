<!DOCTYPE html>
<html>
    <head>
        <title>SpringBoot CRUD</title>
        <style>
            table {
                font-family: arial, sans-serif;
                border-collapse: collapse;
             width: 100%;
            }

            td, th {
                border: 1px solid #dddddd;
                text-align: left;
                padding: 8px;
            }

            tr:nth-child(even) {
                background-color: #dddddd;
            }
        </style>
    </head>
<body>

<h2>HTML Table</h2>

<table>
  <tr>
    <th>id</th>
    <th>id_outbox</th>
    <th>id_inbox</th>
    <th>id_problem</th>
    <th>id_karyawan</th>
    <th>id_handling</th>
    <th>id_pelanggan</th>
  </tr>
    <c:forEach var = "list" items="${lists}">
  <tr>
    <td>${list.id}</td>
    <td>${list.id_outbox}</td>
    <td>${list.id_inbox}</td>
    <td>${list.id_problem}</td>
    <td>${list.id_karyawan}</td>
    <td>${list.id_handling}</td>
    <td>${list.id_pelanggan}</td>
  </tr>
  </c:forEach>
</table>

</body>
</html>
