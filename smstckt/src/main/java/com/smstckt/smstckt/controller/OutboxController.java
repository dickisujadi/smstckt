package com.smstckt.smstckt.controller;

import com.smstckt.smstckt.exception.ResourceNotFoundException;
import com.smstckt.smstckt.model.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api")
public class OutboxController {
    @Autowired
    OutboxRepo outboxRepo;

    //Get All Pelanggan data
    @GetMapping("/outbox")
    public List<Outbox> getAllOutboxes(){
        return outboxRepo.findAll();
    }

    //Create a new Outbox
    @PostMapping("/outbox")
    public Outbox createOutbox(@Valid @RequestBody Outbox outbox){
        return outboxRepo.save(outbox);
    }
}
