package com.smstckt.smstckt.controller;

import com.smstckt.smstckt.exception.ResourceNotFoundException;
import com.smstckt.smstckt.model.Pelanggan;
import com.smstckt.smstckt.model.PelangganRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api")
public class PelangganController {

    @Autowired
    PelangganRepo pelangganRepo;

    //Get All Pelanggan data
    @GetMapping("/pelanggan")
    public List<Pelanggan> getAllPelanggan(){
        return pelangganRepo.findAll();
    }

    //Create a new Pelanggan
    @PostMapping("/pelanggan")
    public Pelanggan createPelanggan(@Valid @RequestBody Pelanggan pelanggan){
        return pelangganRepo.save(pelanggan);
    }
}
