package com.smstckt.smstckt.controller;

import com.smstckt.smstckt.exception.ResourceNotFoundException;
import com.smstckt.smstckt.model.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;


@RestController
@RequestMapping("/api")
public class TicketController {

    @Autowired
    TicketRepo ticketRepo;

    @Autowired
    InboxRepo inboxRepo;

    //Get All Ticket data
    @GetMapping("/ticket")
    public List<Ticket> getAllTicket(){
        return ticketRepo.findAll();
    }

    //Create a new Ticket
    @PostMapping("/ticket")
    public Ticket createTicket(@Valid @RequestBody Ticket ticket){
        return ticketRepo.save(ticket);
    }

    //Get a Single Ticket
    @GetMapping("/ticket/{id}")
    public Ticket getTicketById(@PathVariable(value = "id") Integer ticketId){
        return ticketRepo.findById(ticketId).orElseThrow(() -> new ResourceNotFoundException("Ticket", "id", ticketId));
    }

    //Update A Ticket to update statusproceed
    @PutMapping("/ticket/{id}")
    public Ticket updateTicketBy(@PathVariable(value = "id") Integer ticketId,
                                         @Valid @RequestBody Ticket ticketDetails){
        Ticket ticket = ticketRepo.findById(ticketId).orElseThrow(() -> new ResourceNotFoundException("Ticket", "id", ticketId));
        ticket.setId(ticketDetails.getId());
        ticket.setID_outbox(ticketDetails.getID_outbox());
        ticket.setID_inbox(ticketDetails.getID_inbox());
        ticket.setId_problem(ticketDetails.getId_problem());
        ticket.setId_karyawan(ticketDetails.getId_karyawan());
        ticket.setId_handling(ticketDetails.getId_handling());
        ticket.setId_pelanggan(ticketDetails.getId_pelanggan());

        Ticket updatedTicket = ticketRepo.save(ticket);
        return updatedTicket;
    }

    // Delete a Pelanggan
    @DeleteMapping("/ticket/{id}")
    public ResponseEntity<?> deleteTicket(@PathVariable(value = "id") Integer ticketId) {
        Ticket ticket = ticketRepo.findById(ticketId)
                .orElseThrow(() -> new ResourceNotFoundException("Note", "id", ticketId));

        ticketRepo.delete(ticket);

        return ResponseEntity.ok().build();
    }
}
