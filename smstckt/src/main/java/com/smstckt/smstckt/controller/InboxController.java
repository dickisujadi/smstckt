package com.smstckt.smstckt.controller;

import com.smstckt.smstckt.exception.ResourceNotFoundException;
import com.smstckt.smstckt.model.Inbox;
import com.smstckt.smstckt.model.InboxRepo;
import com.smstckt.smstckt.model.Pelanggan;
import com.smstckt.smstckt.model.PelangganRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api")
public class InboxController {

    @Autowired
    InboxRepo inboxRepo;

    //Get All Inbox data
    @GetMapping("/inbox")
    public List<Inbox> getAllInboxes(){
        return inboxRepo.findAll();
    }

    //Update An Inbox to change statusproceed
    @PutMapping("/inbox/{id}")
    public Inbox updateInboxByID(@PathVariable(value = "id") Integer inboxId,
                                         @Valid @RequestBody Inbox inboxDetails){
        Inbox inbox = inboxRepo.findById(inboxId).orElseThrow(() -> new ResourceNotFoundException("Inbox", "id", inboxId));
        inbox.setUpdateindb(inboxDetails.getUpdateindb());
        inbox.setReceivingdatetime(inboxDetails.getReceivingdatetime());
        inbox.setText(inboxDetails.getText());
        inbox.setSenderNumber(inboxDetails.getSenderNumber());
        inbox.setCoding(inboxDetails.getCoding());
        inbox.setUdh(inboxDetails.getUdh());
        inbox.setSmscnumber(inboxDetails.getSmscnumber());
        inbox.setClas(inboxDetails.getClas());
        inbox.setTextDecoded(inboxDetails.getTextDecoded());
        inbox.setId(inboxDetails.getId());
        inbox.setRecipientid(inboxDetails.getRecipientid());
        inbox.setProcessed(inboxDetails.getProcessed());

        Inbox updatedInbox = inboxRepo.save(inbox);
        return updatedInbox;
    }
}
