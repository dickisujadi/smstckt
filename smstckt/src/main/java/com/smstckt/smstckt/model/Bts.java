package com.smstckt.smstckt.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "pelanggan")
@EntityListeners(AuditingEntityListener.class)
@JsonIgnoreProperties(value = {"createdAt", "updateAt"}, allowGetters = true)
public class Bts implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    public int id;

    @Column(name = "name_bts")
    public String name_bts;

    @Column(name = "sector")
    public String sector;

    @Column(name = "region")
    public String region;

    public Bts() {
    }

    public Bts(String name_bts, String sector, String region) {
        this.name_bts = name_bts;
        this.sector = sector;
        this.region = region;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName_bts() {
        return name_bts;
    }

    public void setName_bts(String name_bts) {
        this.name_bts = name_bts;
    }

    public String getSector() {
        return sector;
    }

    public void setSector(String sector) {
        this.sector = sector;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }
}
