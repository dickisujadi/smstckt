package com.smstckt.smstckt.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.sql.Time;
import java.sql.Timestamp;

//import org.apache.jasper.compiler.ELNode;

@Entity
@Table(name = "outbox")
@EntityListeners(AuditingEntityListener.class)
@JsonIgnoreProperties(value = {"createdAt", "updateAt"}, allowGetters = true)
public class Outbox {

    @Column(name = "updatedindb")
    public Timestamp updateindb;

    @Column(name = "insertintodb")
    public Timestamp insertintodb;

    @Column(name = "sendingdatetime")
    public Timestamp sendingdatetime;

    @Column(name = "sendbefore")
    public Time sendbefore;

    @Column(name = "sendafter")
    public Time sendafter;

    @Column(name = "text")
    public String text;

    @Column(name = "destinationnumber", columnDefinition = "varchar", nullable = false, unique = true)
    public String destinationnumber;

    @Column(name = "coding")
    public String coding;

    @Column(name = "udh")
    public String udh;

//    @Column(name = "smscnumber")
//    public String smscnumber;

    @Column(name = "class")
    public int clas;

    @Column(name = "textdecoded", columnDefinition = "text")
    public String textDecoded;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    public int id;

    @Column(name = "multipart")
    public String multipart;

    @Column(name = "relativevalidity")
    public int relativevalidty;

    @Column(name = "senderid")
    public String senderid;

    @Column(name = "sendingtimeout")
    public Timestamp sendingtimeout;

    @Column(name = "deliveryreport")
    public String deliveryreport;

    @Column(name = "creatorid")
    public String creatorid;

    public Outbox() {
    }

    public Outbox(Timestamp updateindb, Timestamp insertintodb, Timestamp sendingdatetime, Time sendbefore, Time sendafter, String text, String destinationnumber, String coding, String udh, int clas, String textDecoded, String multipart, int relativevalidty, String senderid, Timestamp sendingtimeout, String deliveryreport, String creatorid) {
        this.updateindb = updateindb;
        this.insertintodb = insertintodb;
        this.sendingdatetime = sendingdatetime;
        this.sendbefore = sendbefore;
        this.sendafter = sendafter;
        this.text = text;
        this.destinationnumber = destinationnumber;
        this.coding = coding;
        this.udh = udh;
        this.clas = clas;
        this.textDecoded = textDecoded;
        this.multipart = multipart;
        this.relativevalidty = relativevalidty;
        this.senderid = senderid;
        this.sendingtimeout = sendingtimeout;
        this.deliveryreport = deliveryreport;
        this.creatorid = creatorid;
    }

    public Timestamp getUpdateindb() {
        return updateindb;
    }

    public void setUpdateindb(Timestamp updateindb) {
        this.updateindb = updateindb;
    }

    public Timestamp getInsertintodb() {
        return insertintodb;
    }

    public void setInsertintodb(Timestamp insertintodb) {
        this.insertintodb = insertintodb;
    }

    public Timestamp getSendingdatetime() {
        return sendingdatetime;
    }

    public void setSendingdatetime(Timestamp sendingdatetime) {
        this.sendingdatetime = sendingdatetime;
    }

    public Time getSendbefore() {
        return sendbefore;
    }

    public void setSendbefore(Time sendbefore) {
        this.sendbefore = sendbefore;
    }

    public Time getSendafter() {
        return sendafter;
    }

    public void setSendafter(Time sendafter) {
        this.sendafter = sendafter;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getDestinationnumber() {
        return destinationnumber;
    }

    public void setDestinationnumber(String destinationnumber) {
        this.destinationnumber = destinationnumber;
    }

    public String getCoding() {
        return coding;
    }

    public void setCoding(String coding) {
        this.coding = coding;
    }

    public String getUdh() {
        return udh;
    }

    public void setUdh(String udh) {
        this.udh = udh;
    }

    public int getClas() {
        return clas;
    }

    public void setClas(int clas) {
        this.clas = clas;
    }

    public String getTextDecoded() {
        return textDecoded;
    }

    public void setTextDecoded(String textDecoded) {
        this.textDecoded = textDecoded;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getMultipart() {
        return multipart;
    }

    public void setMultipart(String multipart) {
        this.multipart = multipart;
    }

    public int getRelativevalidty() {
        return relativevalidty;
    }

    public void setRelativevalidty(int relativevalidty) {
        this.relativevalidty = relativevalidty;
    }

    public String getSenderid() {
        return senderid;
    }

    public void setSenderid(String senderid) {
        this.senderid = senderid;
    }

    public Timestamp getSendingtimeout() {
        return sendingtimeout;
    }

    public void setSendingtimeout(Timestamp sendingtimeout) {
        this.sendingtimeout = sendingtimeout;
    }

    public String getDeliveryreport() {
        return deliveryreport;
    }

    public void setDeliveryreport(String deliveryreport) {
        this.deliveryreport = deliveryreport;
    }

    public String getCreatorid() {
        return creatorid;
    }

    public void setCreatorid(String creatorid) {
        this.creatorid = creatorid;
    }
}
