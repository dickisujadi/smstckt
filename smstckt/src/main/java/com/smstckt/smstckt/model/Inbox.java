package com.smstckt.smstckt.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
//import org.apache.jasper.compiler.ELNode;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import javax.websocket.Decoder;
import java.sql.Timestamp;

@Entity
@Table(name = "inbox")
@EntityListeners(AuditingEntityListener.class)
@JsonIgnoreProperties(value = {"createdAt", "updateAt"}, allowGetters = true)
public class Inbox {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    public int id;

    @Column(name = "textdecoded", columnDefinition = "text")
    public String textDecoded;

    @Column(name = "sendernumber", columnDefinition = "varchar", nullable = false, unique = true)
    public String senderNumber;

    @Column(name = "updatedindb")
    public Timestamp updateindb;

    @Column(name = "receivingdatetime")
    public Timestamp receivingdatetime;

    @Column(name = "text")
    public String text;

    @Column(name = "coding")
    public String coding;

    @Column(name = "udh")
    public String udh;

    @Column(name = "smscnumber")
    public String smscnumber;

    @Column(name = "class")
    public int clas;

    @Column(name = "recipientid")
    public String recipientid;

    @Column(name = "processed")
    public String processed;

    @Column(name = "statusproceed")
    public int statusproceed;

    public Inbox() {
    }

    public Inbox(String textDecoded, String senderNumber, Timestamp updateindb, Timestamp receivingdatetime, String text, String coding, String udh, String smscnumber, int clas, String recipientid, String processed, int statusproceed) {
        this.textDecoded = textDecoded;
        this.senderNumber = senderNumber;
        this.updateindb = updateindb;
        this.receivingdatetime = receivingdatetime;
        this.text = text;
        this.coding = coding;
        this.udh = udh;
        this.smscnumber = smscnumber;
        this.clas = clas;
        this.recipientid = recipientid;
        this.processed = processed;
        this.statusproceed = statusproceed;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTextDecoded() {
        return textDecoded;
    }

    public void setTextDecoded(String textDecoded) {
        this.textDecoded = textDecoded;
    }

    public String getSenderNumber() {
        return senderNumber;
    }

    public void setSenderNumber(String senderNumber) {
        this.senderNumber = senderNumber;
    }

    public Timestamp getUpdateindb() {
        return updateindb;
    }

    public void setUpdateindb(Timestamp updateindb) {
        this.updateindb = updateindb;
    }

    public Timestamp getReceivingdatetime() {
        return receivingdatetime;
    }

    public void setReceivingdatetime(Timestamp receivingdatetime) {
        this.receivingdatetime = receivingdatetime;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getCoding() {
        return coding;
    }

    public void setCoding(String coding) {
        this.coding = coding;
    }

    public String getUdh() {
        return udh;
    }

    public void setUdh(String udh) {
        this.udh = udh;
    }

    public String getSmscnumber() {
        return smscnumber;
    }

    public void setSmscnumber(String smscnumber) {
        this.smscnumber = smscnumber;
    }

    public int getClas() {
        return clas;
    }

    public void setClas(int clas) {
        this.clas = clas;
    }

    public String getRecipientid() {
        return recipientid;
    }

    public void setRecipientid(String recipientid) {
        this.recipientid = recipientid;
    }

    public String getProcessed() {
        return processed;
    }

    public void setProcessed(String processed) {
        this.processed = processed;
    }

    public int getStatusproceed() {
        return statusproceed;
    }

    public void setStatusproceed(int statusproceed) {
        this.statusproceed = statusproceed;
    }
}
