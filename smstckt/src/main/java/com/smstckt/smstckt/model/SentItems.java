package com.smstckt.smstckt.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.sql.Time;
import java.sql.Timestamp;

//import org.apache.jasper.compiler.ELNode;

@Entity
@Table(name = "outbox")
@EntityListeners(AuditingEntityListener.class)
@JsonIgnoreProperties(value = {"createdAt", "updateAt"}, allowGetters = true)
public class SentItems {

    @Column(name = "updatedindb")
    public Timestamp updateindb;

    @Column(name = "insertintodb")
    public Timestamp insertintodb;

    @Column(name = "sendingdatetime")
    public Timestamp sendingdatetime;

    @Column(name = "deliverydatetime")
    public Timestamp deliverydatetime;

    @Column(name = "text")
    public String text;

    @Column(name = "destinationnumber", columnDefinition = "varchar", nullable = false, unique = true)
    public String destinationnumber;

    @Column(name = "coding")
    public String coding;

    @Column(name = "udh")
    public String udh;

    @Column(name = "smscnumber")
    public String smscnumber;

    @Column(name = "class")
    public int clas;

    @Column(name = "textdecoded", columnDefinition = "text")
    public String textDecoded;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    public int id;

    @Column(name = "senderid")
    public String senderid;

    @Column(name = "sequenceposition")
    public int sequenceposition;

    @Column(name = "multipart")
    public String multipart;

    @Column(name = "relativevalidty")
    public int relativevalidty;

    @Column(name = "sendingtimeout")
    public Timestamp sendingtimeout;

    @Column(name = "deliveryreport")
    public String deliveryreport;

    @Column(name = "creatorid")
    public String creatorid;
}
