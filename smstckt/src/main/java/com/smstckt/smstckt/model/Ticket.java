package com.smstckt.smstckt.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;

@Entity
@Table(name = "ticket")
@EntityListeners(AuditingEntityListener.class)
@JsonIgnoreProperties(value = {"createdAt", "updateAt"}, allowGetters = true)
public class Ticket {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    public int id;

    @Column(name = "ID_outbox")
    public int ID_outbox;

    @Column(name = "ID_inbox")
    public int ID_inbox;

    @Column(name = "id_problem")
    public int id_problem;

    @Column(name = "id_karyawan")
    public int id_karyawan;

    @Column(name = "id_handling")
    public int id_handling;

    @Column(name = "id_pelanggan")
    public int id_pelanggan;

    public Ticket() {
    }

    public Ticket(int ID_outbox, int ID_inbox, int id_problem, int id_karyawan, int id_handling, int id_pelanggan) {
        this.ID_outbox = ID_outbox;
        this.ID_inbox = ID_inbox;
        this.id_problem = id_problem;
        this.id_karyawan = id_karyawan;
        this.id_handling = id_handling;
        this.id_pelanggan = id_pelanggan;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getID_outbox() {
        return ID_outbox;
    }

    public void setID_outbox(int ID_outbox) {
        this.ID_outbox = ID_outbox;
    }

    public int getID_inbox() {
        return ID_inbox;
    }

    public void setID_inbox(int ID_inbox) {
        this.ID_inbox = ID_inbox;
    }

    public int getId_problem() {
        return id_problem;
    }

    public void setId_problem(int id_problem) {
        this.id_problem = id_problem;
    }

    public int getId_karyawan() {
        return id_karyawan;
    }

    public void setId_karyawan(int id_karyawan) {
        this.id_karyawan = id_karyawan;
    }

    public int getId_handling() {
        return id_handling;
    }

    public void setId_handling(int id_handling) {
        this.id_handling = id_handling;
    }

    public int getId_pelanggan() {
        return id_pelanggan;
    }

    public void setId_pelanggan(int id_pelanggan) {
        this.id_pelanggan = id_pelanggan;
    }
}
