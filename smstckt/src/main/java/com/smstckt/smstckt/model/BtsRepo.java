package com.smstckt.smstckt.model;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface BtsRepo extends JpaRepository<Bts, Integer> {
}
