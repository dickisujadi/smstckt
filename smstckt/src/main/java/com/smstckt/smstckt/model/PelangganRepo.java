package com.smstckt.smstckt.model;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PelangganRepo extends JpaRepository<Pelanggan, Integer> {
}
