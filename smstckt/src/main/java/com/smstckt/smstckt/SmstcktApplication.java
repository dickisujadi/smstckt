package com.smstckt.smstckt;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

@SpringBootApplication
@EnableJpaAuditing
public class SmstcktApplication {

	public static void main(String[] args) {
		SpringApplication.run(SmstcktApplication.class, args);
	}
}
