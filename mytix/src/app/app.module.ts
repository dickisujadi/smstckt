import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { routes } from './app.router';

import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { InboxComponent } from './inbox/inbox.component';
import { OncheckComponent } from './oncheck/oncheck.component';
import { DonecheckComponent } from './donecheck/donecheck.component';
import { SolvedComponent } from './solved/solved.component';
import { ReportComponent } from './report/report.component';
import { CsComponent } from './cs/cs.component';
import { NocComponent } from './noc/noc.component';
import { LoginComponent } from './login/login.component';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { ReportnocComponent } from './reportnoc/reportnoc.component';
import { OpenticketComponent } from './openticket/openticket.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    InboxComponent,
    OncheckComponent,
    DonecheckComponent,
    SolvedComponent,
    ReportComponent,
    CsComponent,
    NocComponent,
    LoginComponent,
    HeaderComponent,
    FooterComponent,
    DashboardComponent,
    ReportnocComponent,
    OpenticketComponent
  ],
  imports: [
    BrowserModule,
	routes
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
