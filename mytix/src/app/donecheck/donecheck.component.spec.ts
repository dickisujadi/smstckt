import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DonecheckComponent } from './donecheck.component';

describe('DonecheckComponent', () => {
  let component: DonecheckComponent;
  let fixture: ComponentFixture<DonecheckComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DonecheckComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DonecheckComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
