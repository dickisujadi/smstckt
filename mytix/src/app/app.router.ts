import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { InboxComponent } from './inbox/inbox.component';
import { OncheckComponent } from './oncheck/oncheck.component';
import { DonecheckComponent } from './donecheck/donecheck.component';
import { SolvedComponent } from './solved/solved.component';
import { ReportComponent } from './report/report.component';
import { CsComponent } from './cs/cs.component';
import { NocComponent } from './noc/noc.component';
import { LoginComponent } from './login/login.component';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { ReportnocComponent } from './reportnoc/reportnoc.component';
import { OpenticketComponent } from './openticket/openticket.component';

export const router: Routes = [
{ path: '', redirectTo: 'login', pathMatch: 'full'},
{ path: 'login', component: LoginComponent},
{ path: 'home', component: HomeComponent},
{ path: 'inbox', component: InboxComponent},
{ path: 'oncheck', component: OncheckComponent},
{ path: 'donecheck', component: DonecheckComponent},
{ path: 'solved', component: SolvedComponent},
{ path: 'report', component: ReportComponent},
{ path: 'solved', component: SolvedComponent},
{ path: 'cs', component: CsComponent},
{ path: 'noc', component: NocComponent},
{ path: 'header', component: HeaderComponent},
{ path: 'footer', component: FooterComponent},
{ path: 'dashboard', component: DashboardComponent},
{ path: 'reportnoc', component: ReportnocComponent},
{ path: 'openticket', component: OpenticketComponent}
];

export const routes: ModuleWithProviders = RouterModule.forRoot(router);
