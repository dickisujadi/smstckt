import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OncheckComponent } from './oncheck.component';

describe('OncheckComponent', () => {
  let component: OncheckComponent;
  let fixture: ComponentFixture<OncheckComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OncheckComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OncheckComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
