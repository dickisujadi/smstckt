import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReportnocComponent } from './reportnoc.component';

describe('ReportnocComponent', () => {
  let component: ReportnocComponent;
  let fixture: ComponentFixture<ReportnocComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReportnocComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReportnocComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
